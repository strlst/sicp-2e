; approximation sine function
(define (p x) 
    (define (cube x) (* x x x))
    (- (* 3 x) (* 4 (cube x))))

(define (sine ang)
    (if (not (> (abs ang) 0.1))
        ang
        (p (sine (/ ang 3.0)))))

(define (sine-step-count ang step)
    (if (not (> (abs ang) 0.1))
        (begin (display step) (newline) ang)
        (p (sine-step-count (/ ang 3.0) (+ step 1)))))

; procedure expansion
(sine 12.15)
(p (sine 4.05))
(p (p (sine 1.3499999999999999)))
(p (p (p (sine 0.44999999999999996))))
(p (p (p (p (sine 0.15)))))
(p (p (p (p (p (sine 0.049999999999999996))))))
(p (p (p (p (p 0.049999999999999996)))))
(p (p (p (p 0.1495))))
(p (p (p 0.4351345505)))
(p (p 0.9758465331678772))
(p -0.7895631144708228)

; based on the above, it can be inferred that sine does not expand geometrically, but iteratively
; for 12.15 specifically, there are 6 iteration steps

(sine 12.15)
(sine-step-count 12.15 1)
; sine step count 6
(sine 121.5)
(sine-step-count 121.5 1)
; sine step count 8
(sine 1215)
(sine-step-count 1215 1)
; sine step count 10

; a relationship like [ n * 10 = initial steps + 2 ] is typically associated with logarithmic growth
; imagine a ruler with a logarithmic scale, where...
; - the distance between 1 and 3 equals...
; - the distance between 3 and 9 equals...
; - the distance between 9 and 27 equals...
; ad infinitum

; we can logically infer a theta of theta(log(n))

; space = steps (?)
; probably
