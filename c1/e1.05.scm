; normal order evaluation:
;   - would expand values until it reaches a primitive, upon which it would expand arguments for final evaluation (which, because of evaluation 'relevance', forgoes evaluation of the the expression "(p)")
; applicative order evaluation:
;   - would hang, as the expression "(p)" infinitely evaluates to itself
;   - operands are evaluated, not expanded
