; use newtons better approximation formula
; (x / y^2 + 2y) / 3

(define (sqrt-iter prev-guess guess x)
; util procs
    (define (square x) (* x x))
; ??? somehow throws up invalid values
    (define (approx g)
        (/ (+ (/ x (square g))
              (* 2 g))
            3))
    (define (delta prev-guess guess)
        (abs (- prev-guess guess)))
; body
    (if (< (delta prev-guess guess) (* prev-guess 0.0000001))
            guess
            (sqrt-iter guess (approx guess) x)))

(define (sqrt x) (sqrt-iter 0. 1. x))

3                  (sqrt 3)
4                  (sqrt 4)
9                  (sqrt 9)
100                (sqrt 100)
400000000          (sqrt 400000000)
0.1                (sqrt 0.1)
0.0000001          (sqrt 0.0000001)
123456789123456789 (sqrt 123456789123456789)
.98765432198765432 (sqrt .98765432198765432)