; resolution is shown with two recursion trees, (not meant to be run as code)

; variant a
(define (+ a b)
    (if (= a 0)
        b
        (inc (+ (dec a) b))))
(+ 4 5)
(inc (+ (dec 4) 5))
(inc (inc (+ (dec 3) 5)))
(inc (inc (inc (+ (dec 2) 5))))
(inc (inc (inc (inc (+ (dec 1) 5)))))
(inc (inc (inc (inc (+ 0 5)))))
(inc (inc (inc (inc 5))))
(inc (inc (inc 6)))
(inc (inc 7))
(inc 8)
9

; thus, this form of addition procedure is a linear recursive process

; variant b
(define (+ a b)
    (if (= a 0)
        b
        (+ (dec a) (inc b))))
(+ 4 5)
(+ (dec 4) (inc 5))
(+ (dec 3) (inc 6))
(+ (dec 2) (inc 7))
(+ (dec 1) (inc 8))
(+ 0 9)
9

; thus, this form of addition procedure is a linear iterative process
; (while it assumes the form of a typical recursion function, the process uses 'tail-recusion', such that a state is fully contained within the parameters of the process, thus not requiring to call back in typical recursive fashion)
