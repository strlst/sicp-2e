(define (A x y)
    (cond ((= y 0) 0)
          ((= x 0) (* 2 y))
          ((= y 1) 2)
          (else (A (- x 1)
                   (A x (- y 1))))))

"(define (g n) (A 1 n))"
; simple case, expands identically until recursion base case (= y 1) is reached, then evaluates according to second rule
(A 1 10)
(A 0 (A 1 9))
(A 0 (A 0 (A 1 8)))
(A 0 (A 0 (A 0 (A 1 7))))
(A 0 (A 0 (A 0 (A 0 (A 1 6)))))
(A 0 (A 0 (A 0 (A 0 (A 0 (A 1 5))))))
(A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 1 4)))))))
(A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 1 3))))))))
(A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 1 2)))))))))
(A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 1 1))))))))))
(A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 2)))))))))
(A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 4))))))))
(A 0 (A 0 (A 0 (A 0 (A 0 (A 0 (A 0 8)))))))
(A 0 (A 0 (A 0 (A 0 (A 0 (A 0 16))))))
(A 0 (A 0 (A 0 (A 0 (A 0 32)))))
(A 0 (A 0 (A 0 (A 0 64))))
(A 0 (A 0 (A 0 128)))
(A 0 (A 0 256))
(A 0 512)
1024
; thus, for (define (g n) (A 1 n)) where x is always 1, (g n) computes 2^n

"(define (f n) (A 0 n))"
(A 0 3)
; similarly, for (define (f n) (A 0 n)), as x is always 0, (f n) produces 2n

"(define (h n) (A 2 n))"
(A 2 4)
(A 1 (A 2 3))
(A 1 (A 1 (A 2 2)))
(A 1 (A 1 (A 1 (A 2 1))))       ; y base case
(A 1 (A 1 (A 1 2)))
(A 1 (A 1 (A 0 (A 1 1))))       ; y base case
(A 1 (A 1 (A 0 2)))             ; x base case start/end
(A 1 (A 1 4))
(A 1 (A 0 (A 1 3)))
(A 1 (A 0 (A 0 (A 1 2))))
(A 1 (A 0 (A 0 (A 0 (A 1 1))))) ; y base case
(A 1 (A 0 (A 0 (A 0 2))))       ; x base case start
(A 1 (A 0 (A 0 4)))
(A 1 (A 0 8))                   ; x base case end
(A 1 16)
; at this point (g n) takes over and produces the result of 2^16
; our function has the form (define (h n) (A 2 n))
; a reasonable guess would be that the n used in (g n) is produced by (h n) in the form of n^2, such that the final closed form would yield 2^(n^2) FALSE for (A 2 3) => 16
; (A 2 5) yields a stack overflow, precisely at (A 1 57430) [a stupidly large exponent to 2]
; the actual mathematical relationship is that the power of 2 is taken n times iteratively, representing a so called hyperoperation
;    for (A 2 3) -> 2^2^2
;    for (A 2 4) -> 2^2^2^2
;    for (A 2 5) -> 2^2^2^2^2
; this concept can be accurately represented using Knuth's up-arrow notation, such that 2 ↑ n (2 up n)
(A 3 3)
