; define a procedure that takes three numbers as args and returns sum of squares of the two larger numbers

(define (square n) (* n n))
(define (square-sum x y) (+ (square x) (square y)))
(define (biggest-square-sum x y z)
    (square-sum
        (max (max x y)
              z)
        (min (max x y)
             (max y z)
             (max x z))))

(display "\
test cases:\n\
1 2 3 -> ") (biggest-square-sum 1 2 3)
(display "\
1 3 2 -> ") (biggest-square-sum 1 3 2)
(display "\
2 1 3 -> ") (biggest-square-sum 2 1 3)
(display "\
2 3 1 -> ") (biggest-square-sum 2 3 1)
(display "\
3 1 2 -> ") (biggest-square-sum 3 1 2)
(display "\
3 2 1 -> ") (biggest-square-sum 3 2 1)
