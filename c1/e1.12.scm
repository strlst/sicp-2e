; pascal's triangle
; simple recursive process
; - border nodes (x <= 1 && x >= y) are by definition 1
; - other nodes are simply the sum of two above nodes, which both have a y component of (- y 1) and x components of x and (- x 1) respectively
; following these simple statements, simple (fault-tolerant) recursion rules can be applied

(define (pascal x y)
    (cond ((< x 2      ) 1)
          ((> x (- y 1)) 1)
          (else (+ (pascal (- x 1) (- y 1))
                   (pascal x       (- y 1))))))

(pascal 1 1)
(pascal 1 2)
(pascal 2 2)
(pascal 1 3)
(pascal 2 3)
(pascal 3 3)
(pascal 2 5)
(pascal 3 5)
(pascal 4 5)
(pascal 6 10)
(pascal 9 15)
(pascal 8 15)
(pascal 7 15)
(pascal 6 15)
; works
