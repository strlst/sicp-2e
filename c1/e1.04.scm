; when (> b 0) evaluates to #t, that is, when b is larger htan 0, the procedure applies addition or subtraction to operands a and b
; on #t -> + a b
; on #f -> - a b
