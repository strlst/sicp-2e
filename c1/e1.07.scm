; make 'good-enough?' utilize a differential error check instead of checking against the square of its own result
; testing at the same EPSILON, no noticeable difference in accuracy to 1.6 version

(define (sqrt-iter prev-guess guess x)
; util procs
    (define (average x y) (/ (+ x y) 2))
; backbone procs of sqrt-iter
    (define (improve guess) (average guess (/ x guess)))
    (define (delta prev-guess guess)
        (abs (- prev-guess guess)))
; body
    (if (< (delta prev-guess guess) 0.0000001)
            guess
            (sqrt-iter guess (improve guess) x)))

(define (sqrt x) (sqrt-iter 0. 1. x))

3                  (sqrt 3)
4                  (sqrt 4)
9                  (sqrt 9)
100                (sqrt 100)
400000000          (sqrt 400000000)
0.1                (sqrt 0.1)
0.0000001          (sqrt 0.0000001)
123456789123456789 (sqrt 123456789123456789)
.98765432198765432 (sqrt .98765432198765432)
