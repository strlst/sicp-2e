; alternative 'if' procedure is implemented, what happens

; util
(define (square x) (* x x))
(define (average x y) (/ (+ x y) 2))

; 'new-if' is evaluated in applicative order, thus the else clause infinitely expands into itself and is never fully evaluated, as such, the process hangs
; similar to 1.5
;(define (new-if predicate then-clause else-clause)
;    (cond (predicate then-clause)
;          (else else-clause)))

(define (good-enough? guess x)
    (< (abs (- (square guess)
               x))
       0.0000001))
(define (improve guess x) (average guess (/ x guess)))
(define (sqrt-iter guess x)
    (if (good-enough? guess x)
            guess
            (sqrt-iter (improve guess x) x)))

(define (sqrt x) (sqrt-iter 1. x))

3                  (sqrt 3)
4                  (sqrt 4)
9                  (sqrt 9)
100                (sqrt 100)
400000000          (sqrt 400000000)
0.1                (sqrt 0.1)
0.0000001          (sqrt 0.0000001)
123456789123456789 (sqrt 123456789123456789)
.98765432198765432 (sqrt .98765432198765432)
